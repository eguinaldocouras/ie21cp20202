---
title: "Introdução"
date: 2021-05-11T23:59:23-03:00
draft: false

---
![](https://gitlab.com/eguinaldocouras/ie21cp20202/-/raw/master/Docs/static/Infogr%C3%A1fico%20sobre%20o%20mar.png)
---
## Inspirações
A iniciativa de nosso projeto surgiu a partir da necessidade de explorar uma área vasta com um potencial enorme de descobrimentos, que é o universo subaquático,  possuindo cerca de 90% de sua área inexplorada. Após décadas de pesquisas conhecemos apenas 1/3 da biodiversidade marinha, também estima-se que cerca de US$ 771 trilhões em ouro estão perdidos no fundo do mar. Além disso, pode ser que existam mais artefatos submersos do que em museus. 
Tendo isso em vista, decidimos iniciar um protótipo capaz de suprir e quem sabe revolucionar a maneira que explorar nossos mares.

## Objetivos iniciais
O objetivo principal no desenvolvimento desse projeto é simular um sistema subnáutico, para realização de tarefas submarinas, como a observação de um ecossistema, pesquisas ou lazer.

---

## Objetivos Futuros
Tendo em vista a complexidade do projeto e a falta de recursos para exercer algumas das funcionalidades que não puderam ser implementadas, então um dos objetivos para o futuro do nosso projeto é torna-lo autonomo de suas funcionalidades.

---
O sistema elaborado pode ser controlado por um controle remoto em que:

* Possui 4 velocidades, onde o Vol+ orienta devagar para frente, e os botões de 1 a 3 regulam o nível velocidade.
* Existe também o direcionamento do sistema, utilizando as setas dos lados, seja para a direita ou para a esquerda.
* As setas para cima e para baixo controlam sua profundidade.

Tendo em vista de que ficará vulnerável a qualquer tipo de fenômeno, uma vez que estará na água, foram desenvolvidos:

* Luzes frontais e traseiras, que ativam sozinhas quando há baixa luminosidade.
* Uma luz de colisão frontal será ativada, caso algum objeto esteja a menos de 50 cm do sistema.

---

## Materiais utilizados
É possível observar a seguir, os materiais utilizados na construção deste projeto:

| Componente | Quantidade |
| ---------- | ---------- |
| Arduino Uno R3 | 1 |
| Placa de Ensaio Pequena | 1 |
| Motor CC | 2 |
| Posicional Micro servo | 3 |
| Sensor de Distância Ultrasônico HC-SR04 | 1 |
| Fotorresistor | 1 |
| Sensor de Infravermelho | 1 |
| Infravermelho Remoto | 1 |
| Sensor de Inclinação | 1 |
| Resistor 220Ω | 6 |
| Resistor 10kΩ| 1 |
| LED | 5 |

---

## Diagrama Elétrico

[![Esquema elétrico](https://gitlab.com/eguinaldocouras/ie21cp20202/-/raw/master/Docs/static/esquema.png)](https://gitlab.com/eguinaldocouras/ie21cp20202/-/raw/master/Docs/static/esquema.png)

---

## Código

```cpp
#include <Servo.h>
#include <IRremote.h>

#define LDR A5 // Fotoresistor
#define IR 4 // Infra vermelho
#define SR1 9 // Servo direção
#define SR2 10 // Servo profundidade
#define SR3 2 // Servo profundidade
#define MT1 5 //Motor1
#define MT2 6 //Motor2
#define LED1 13 //Led noturno
#define LED2 12 //
#define LED3 11 //
#define TRIG 8 //Sensor colisão
#define ECHO 7 //
#define ALAR1 A3 //Led colisão
#define INC 3 //Sendo inclinação
#define ALAR2 A4 //Led inclinação

Servo servo1; //Objeto servo
Servo servo2;
Servo servo3;

IRrecv receptor(IR); 
decode_results resultado;
bool ligado = false;


void setup()
{
  servo1.attach(SR1);
  servo2.attach(SR2);
  servo3.attach(SR3);
  receptor.enableIRIn();
  Serial.begin(9600);
  
  pinMode(MT1, OUTPUT);//Motores
  pinMode(MT2, OUTPUT);
  
  pinMode(LED1, OUTPUT);//Lâmpadas de posição
  pinMode(LED2, OUTPUT);
  pinMode(LED3, OUTPUT);
  
  pinMode(TRIG, OUTPUT);//Sensor de colisão
  pinMode(ECHO, INPUT);
  digitalWrite(TRIG, LOW);
  pinMode(ALAR1, OUTPUT);
  
  pinMode(INC, INPUT);//Sensor de inclinação
  pinMode(ALAR2, OUTPUT);
  
  servo1.write(90);
  servo2.write(90);
  servo3.write(90);
  
}
			//FD609F DIREITA 45º
			//FD20DF ESQUERDA 135º
			//FD50AF SUBIR
			//FD10EF DESCER
			//FD807F PRA FRENTE
			//FD08F7 1
			//FD8877 2
			//FD48B7 3
			//ligado == 30 +- 650rpm
			//vel1   == 50 +- 1500rpm
			//vel2   == 120 +- 2600rpm
			//vel3 	 == 220 +- 4800rpm

void loop() {
  
  int leitura = analogRead(LDR); // Luz noturna
  
  bool inclinado = digitalRead(INC);//Alarme inclinação
  
  	digitalWrite(TRIG, HIGH); //Alarme de colisão
	delayMicroseconds(10);
	digitalWrite(TRIG, LOW);
  
	int tempo = pulseIn(ECHO, HIGH);  
	float dist = (tempo * 343.0)/20000; 
  
  if (dist < 70 && dist >= 50){ 	//Alarme de colisão
      digitalWrite(ALAR1, HIGH);
      delay(800);
      digitalWrite(ALAR1, LOW);
      delay(800);
  } 
  if (dist < 50 && dist >=25){ 	//Alarme de colisão
      digitalWrite(ALAR1, HIGH);
      delay(400);
      digitalWrite(ALAR1, LOW);
      delay(400);
  } 
  if (dist < 25){ 	//Alarme de colisão
      digitalWrite(ALAR1, HIGH);
      delay(200);
      digitalWrite(ALAR1, LOW);
      delay(200);
  } 
  else{//Alrme de colisão
    digitalWrite(ALAR1, LOW);
  }	
  
  if (!inclinado){ //Alarme de inclinação
	  digitalWrite(ALAR2, HIGH);
      delay(200);
      digitalWrite(ALAR2, LOW);
      delay(200);
  }
  else{//Alarme de inclinaçã
    digitalWrite(ALAR2, LOW);
  }  

  
  
  if(leitura>140){				//Luz Noturna
   	digitalWrite(LED1, HIGH);
    digitalWrite(LED2, HIGH); 
    digitalWrite(LED3, HIGH); 
  }
  else{
    digitalWrite(LED1, LOW);
    digitalWrite(LED2, LOW); 
    digitalWrite(LED3, LOW); 
  }								//Luz noturna
  	  
	if (receptor.decode(&resultado)) {
      Serial.println(resultado.value, HEX);
	
      	switch (resultado.value) {
        
        case 0xFD807F : // frente
        
            analogWrite(MT1, 30);
  			analogWrite(MT2, 30);
        	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
          
         case 0xFD08F7 : // vel 1          
            analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
                    
         case 0xFD8877 : // vel 2          
            analogWrite(MT1, 110);
  			analogWrite(MT2, 110);
        	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
          
        case 0xFD48B7 : // vel 3         
            analogWrite(MT1, 220);
  			analogWrite(MT2, 220);
          	servo1.write(90);
            servo2.write(90);
            servo3.write(90);
  			delay(10);
          break;
             
        case 0xFD609F : // esquerda
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(135);
  			delay(10);
        break;
        
       case 0xFD20DF : // direita  
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(45);
  			delay(10);
        break;
          
       case 0xFD50AF : // subir 
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(90);
        	servo2.write(45);
        	servo3.write(135);
  			delay(10);
        break;
          
       case 0xFD10EF : // descer
        	analogWrite(MT1, 50);
  			analogWrite(MT2, 50);
        	servo1.write(90);
        	servo2.write(135);
        	servo3.write(45);
  			delay(10);
        break;
          
       default : // neutro
        	analogWrite(MT1, 0);
  			analogWrite(MT2, 0);
        	servo1.write(90);
        	servo2.write(90);
        	servo3.write(90);
  			delay(10);
        break;
          
      }
        
        receptor.resume();       
 }
}

```

---

## Resultados
No vídeo abaixo, podemos observar uma demonstração de como o projeto funciona:

{{< youtube Izhel9KyucQ >}}

---

* Também é possível entrar no projeto pelo link:
[Tinkercad - Duck Dodgers](https://www.tinkercad.com/things/i58EFLbjDey-projetin/editel?sharecode=cYHBkNMGzjgJomEU6IFb352ruECnX61jgaqgLHDFXQ0)

---

## Desafios Encontrados

* Durante a elaboração do projeto foram encontrados alguns desafios, como as limitaçoes do Arduino e principalmente a plataforma Tinkercad, mas o maior desafio encontrado e que completamente inesperado foi a elaboração do site, em que diversos bugs e problemas apareceram.

