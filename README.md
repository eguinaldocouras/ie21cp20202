# Introdução
O projeto foi desenvolvido pelos alunos de Engenharia de Computação da Universidade Tecnológica Federal do Paraná (UTFPR).

#### Esquema de navegação simulado por IR

![Esquema Duck Dodger](Docs/static/esquema.png)     

## Equipe

| Nome | gitlab user |
| ---- | ----------- |
| Eguinaldo Couras | @eguinaldocouras |
| Julio V Perin| @JulioPerin |
| Maria Clara de Lima | @Lclara |

## Documentação
A documentação pode ser acessada [clicando aqui.](https://eguinaldocouras.gitlab.io/ie21cp20202).
# Links Úteis

* [Kit de Desenvolvimento Arduino](https://www.arduino.cc/)
* [Sintax Markdown](https://docs.gitlab.com/ee/user/markdown.html)
* [Hugo Website](https://gohugo.io/)

